#!/usr/bin/env python3

import os
import sys
import sqlite3
import argparse
import pandas as pd

# from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
# from prometheus_client.openmetrics import parser

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class ConvertData():
    """
    """

    def __init__(self):
        """
        """
        self.args = {}
        self.parse_args()

        self.database = self.args.database

        self.fields = [
             'weight',
             'fat',
             'water',
             'muscle',
             'visceralFat',
             'lbm',
             'waist',
             'hip',
             'bone'
           ]

        # "SELECT datetime,weight,fat,water,muscle,visceralFat,lbm,waist,hip,bone FROM scaleMeasurements where enabled = '1' order by datetime"
        self.query = f"select datetime, {','.join(self.fields)} from scaleMeasurements where enabled = '1' order by datetime "

    def parse_args(self):
        """
        """
        p = argparse.ArgumentParser(description='convert openScale Sqlite to prometheus output')

        p.add_argument(
            "-d",
            "--database",
            required=True,
            help="databasefile",
            default='openScale.db'
        )

        self.args = p.parse_args()

    def convert(self):
        """
        """

        if not os.path.exists(self.database):
            """
            """
            print(f"database file {self.database} dosen't exists")
            sys.exit(1)

        # self.__convert_to_csv()
        self.__convert_to_openmetrics()

        sys.exit(0)

    def __convert_to_csv(self):
        """
        """
        conn = sqlite3.connect(
            self.database,
            isolation_level=None,
            detect_types=sqlite3.PARSE_COLNAMES
        )
        db_df = pd.read_sql_query(
            self.query,
            conn
        )
        db_df.to_csv('database.csv', index=False)

    def __convert_to_openmetrics(self):
        """
            id,userId,enabled,datetime,weight,fat,water,muscle,visceralFat,lbm,waist,hip,bone,chest,thigh,biceps,neck,caliper1,caliper2,caliper3,calories,comment
        """

        connection = sqlite3.connect(self.database)
        connection.row_factory = lambda c, r: dict([(col[0], r[idx]) for idx, col in enumerate(c.description)]) # sqlite3.Row
        cursor = connection.cursor()

        file_object = open('openscale.prom', 'a')

        for r in cursor.execute(self.query):
            # print(r)
            for m in self.fields:
                open_metrics = f"open_scale{{\"body\"=\"bodsch\", \"measurement\"=\"{m}\"}} {r.get(m)} {r.get('datetime')}"
                print(open_metrics)
                file_object.write(f"{open_metrics}\n")

        connection.close()
        file_object.close()


if __name__ == '__main__':
    """
    """
    r = ConvertData()
    r.convert()
